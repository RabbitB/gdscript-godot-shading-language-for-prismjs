The purpose of this project is to add GDScript & Godot Shading Language (GSL) highlighting to PrismJS.

## How to Install
There are four source files in this project; two for GDScript & two for GSL. One is a human-readable version that can be easily modified, and one is a compacted version ready to be copied into your `prism.js` file. Nothing additional needs to be done to have Prism recognize the new languages; simply copy the contents of the compact source files and append them as new lines into your `prism.js` file.

## Theming

The `prism.css` file you generated when you downloaded Prism doesn't do enough to differentiate different code entities. That's why these language definitions define a couple of new categories that will need to be added to `prism.css` to style them.

```
builtin_function
builtin_class
```

Be aware that the GSL language definition defines built-in variables (VERTEX, COLOR, SCREEN_UV, etc.) under `builtin_class`. This is because GSL lacks classes, and instead of creating yet another category, it simply uses what would otherwise be an empty category.

LICENSED UNDER CC0 (https://creativecommons.org/publicdomain/zero/1.0/)